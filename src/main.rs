use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde_json::Value;
use serde::Serialize;

// #[derive(Deserialize)]
// struct Request {
//     // greeting: String,
//     // number1: i32,
//     // number2: i32,
// }

#[derive(Serialize)]
struct Response {
    req_id: String,
    //msg: String,
    avg: i32,
}

//A marco polo function that return polo if you pass in Marco
// fn marco_polo_greeting(greeting: &str) -> String {
//     if greeting == "Marco" {
//         "Polo".to_string()
//     } else {
//         "No".to_string()
//     }
// }
fn sum_numbers(value: &Value) -> (i32, i32) {
    match value {
        Value::Number(num) => {
            let num = num.as_i64().unwrap_or(0) as i32;
            (num, 1)
        },
        Value::Array(arr) => arr.iter().map(sum_numbers).fold((0, 0), |acc, x| (acc.0 + x.0, acc.1 + x.1)),
        Value::Object(obj) => obj.values().map(sum_numbers).fold((0, 0), |acc, x| (acc.0 + x.0, acc.1 + x.1)),
        _ => (0, 0),
    }
}
async fn function_handler(event: LambdaEvent<Value>) -> Result<Response, Error> {

    let (sum, count) = sum_numbers(&event.payload);
    let avg = if count > 0 { sum / count } else { 0 };
    let resp = Response {
        req_id: event.context.request_id,
        avg,
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
